<?php
require_once("functions.php");
$myurl = $_SERVER['PHP_SELF']; // Iseviitav link. Vormi andmete saatmiseks tagasi samale scriptile.
connect_db();
start_session();

//Massiiv piltidega kuvamiseks
$pictures = array(
    array(
        "pic" => "pics/img1.png",
        "alt" => "img1"
    ),
    array(
        "pic" => "pics/img2.png",
        "alt" => "img2"
    ),
    array(
        "pic" => "pics/img3.png",
        "alt" => "img3"
    ),
    
    array(
        "pic" => "pics/img4.png",
        "alt" => "img4"
    ),
    array(
        "pic" => "pics/img5.png",
        "alt" => "img5"
    ),
    array(
        "pic" => "pics/img6.png",
        "alt" => "img6"
    ),
    
    array(
        "pic" => "pics/img7.png",
        "alt" => "img7"
    ),
    array(
        "pic" => "pics/img8.png",
        "alt" => "img8"
    ),
    array(
        "pic" => "pics/img9.png",
        "alt" => "img9"
    )
    
);

//Vaikimisi mode 
$mode = "main";
if (isset($_GET['mode']) && $_GET['mode'] != "") {
    $mode = $_GET['mode'];
}

//Kujunduse päis
include_once("view/head.html");

//Vastavalt mode-ile kuvatav info
switch ($mode) {
    case "tvseries":
        tvseries();
        break;
    case "movies":
        movies();
        break;
    case "login":
        login();
        break;
    case "loginauth":
       if (userin()) {
            review();
        }
		else {
			loginauth();
        }
        break;
    case "registerpage":
        registerpage();
        break;
    case "registeruser":
        registeruser();
        break;
    //Prax 10, 2014 arhiiv
    case "logout":
        logout();
        break;
    case "review":
        review();
        break;
	case "delete":
        delete_tvshow();
        break;
    case "addshow":
        if (userin()) {
            addshow();
        }
        break;
    default:
        main();
}
//Kujunduse jalus
include_once("view/foot.html");
?>