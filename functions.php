<?php
// Ühendus andmebaasi
function connect_db()
	{
	global $connection;
	$host = "localhost";
	$user = "test";
	$pass = "t3st3r123";
	$db = "test";
	$connection = mysqli_connect($host, $user, $pass, $db) or die("ei saa ühendust mootoriga- " . mysqli_error());
	mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - " . mysqli_error($connection));
	}
// function connect_db()
// {
// global $connection;
// $host = "localhost";
// $user = "root";
// $pass = "test";
// $db   = "tvshow";
// $connection = mysqli_connect($host, $user, $pass, $db) or die("ei saa mootoriga ühendust");
// mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - " . mysqli_error($connection));
// }
// Vaated lehe kuvamiseks
function main()
	{
	global $myurl;
	include_once ("view/main.html");

	}
function movies()
	{
	global $myurl;
	include_once ("view/movies.html");

	}
function tvseries()
	{
	global $myurl;
	include_once ("view/tvseries.html");

	}
function login()
	{
	global $myurl;
	include_once ("view/login.html");

	}
function registerpage()
	{
	global $myurl;
	include_once ("view/register.html");

	}
function review()
	{
	global $myurl;
	$tvshows = get_tvshows();
	include_once ("view/review.html");

	}
// Sessiooni alustamine ja lõpetamine, õppejõu kood
function start_session()
	{
	// siin võib muuta ka sessioooni kehtivusaega
	session_start();
	}
function endsession()
	{
	$_SESSION = array();
	if (isset($_COOKIE[session_name() ]))
		{
		setcookie(session_name() , '', time() - 42000, '/');
		}
	session_destroy();
	}
// Kasutaja sisselogimise kontroll
function loginauth()
	{
	global $myurl;
	global $connection;
	$errors = array();
	// Kontrollime väljade olemasolu ja täidetust
	if (isset($_POST['username']) && $_POST['username'] !== "")
		{
		$username = $_POST['username'];
		}
	  else
		{
		// Kuvame veateate, juhul kui vorm jäi täitmata
		$errors[] = "Nimi ei saa olla tühi!";
		}
	if (isset($_POST['pword']) && $_POST['pword'] !== "")
		{
		$password = $_POST['pword'];
		}
	  else
		{
		$errors[] = "Parool ei saa olla tühi!";
		}
	if (empty($errors))
		{
		// Kui vigu ei olnud, kontrolli infot
		$username = mysqli_real_escape_string($connection, $username);
		$password = mysqli_real_escape_string($connection, $password);
		$query = "SELECT id, username, privileges FROM jjakimet_kasutajad WHERE username='$username' and pass=sha1('$password')";
		$result = mysqli_query($connection, $query) or die("$query - " . mysqli_error($connection));
		$user = mysqli_fetch_assoc($result);
		if (!empty($user))
			{
			$_SESSION['username'] = $username;
			$_SESSION['user_id'] = $user['id'];
			$_SESSION['privileges'] = $user['privileges'];
			// $_SESSION['notices'][] = "Tere " . htmlspecialchars($username) . ", sisselogimine õnnestus!";
			include ("view/review.html");

			}
		  else
			{
			// vale info puhul kuva uuesti log in leht
			$errors[] = "Kasutajanimi või parool on vale, palun proovi uuesti";
			include ("view/login.html");

			}
		}
	  else
		{
		// Vigade puhul kuva veateated
		include ("view/login.html");

		}
	}
// Kasutaja registreemine
function registeruser()
	{
	global $myurl;
	global $connection;
	$regerrors = array();
	$username = "";
	// Kontrollime väljade täidetust
	if (isset($_POST['username']) && $_POST['username'] != "")
		{
		$username = $_POST['username'];
		}
	  else
		{
		$regerrors[] = "Kasutajanimi puudub!";
		}
	if (isset($_POST['firstname']) && $_POST['firstname'] != "")
		{
		$firstname = $_POST['firstname'];
		}
	  else
		{
		$regerrors[] = "Eesnimi puudub!";
		}
	if (isset($_POST['lastname']) && $_POST['lastname'] != "")
		{
		$lastname = $_POST['lastname'];
		}
	  else
		{
		$regerrors[] = "Perekonnanimi puudub!";
		}
	if (isset($_POST['pword']) && $_POST['pword'] != "")
		{
		$password = $_POST['pword'];
		}
	  else
		{
		$regerrors[] = "Parool puudub!";
		}
	// vigade puudumisel kasutaja lisamine andmebaasi
	if (empty($regerrors))
		{
		$username = mysqli_real_escape_string($connection, $username);
		$password = mysqli_real_escape_string($connection, $password);
		$firstname = mysqli_real_escape_string($connection, $firstname);
		$lastname = mysqli_real_escape_string($connection, $lastname);
		$query = "insert into jjakimet_kasutajad (username, pass, firstname, lastname) values ('$username', sha1('$password'), '$firstname' , '$lastname')";
		mysqli_query($connection, $query) or die("$query - " . mysqli_error($connection));
		$result = mysqli_insert_id($connection);
		if ($result)
			{
			include ("view/login.html");
			exit(0);
			}
		}
	// Vigade puhul kuvame need
	include ("view/register.html");

	}
// Kontrollime kas kasutaja on sees
function userin()
	{
	global $myurl;
	if (isset($_SESSION['username']) && $_SESSION['username'] != "")
		{
		return true;
		}
	  else
		{
		main();
		}
	}
// Pärime tvshowde andmed, abi saadud praktikumimaterjalidest
function get_tvshows()
	{
	global $connection;
	$tvshows = array();
	$userid = $_SESSION['user_id'];
	$query = "SELECT t.id, t.tvshow_title, t.creation_date, k.id as user_id FROM jjakimet_tvshows t, jjakimet_kasutajad k WHERE t.user_id='$userid'";
	$result = mysqli_query($connection, $query) or die("$query - " . mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result))
		{
		$row['id'] = htmlspecialchars($row['id']);
		$row['user_id'] = htmlspecialchars($row['user_id']);
		$row['tvshow_title'] = htmlspecialchars($row['tvshow_title']);
		// Koodi idee pärineb stackoverflowst
		$mysqldate = strtotime($row['creation_date']);
		$date = date('H:i, d.m.y', $mysqldate);
		$row['creation_date'] = htmlspecialchars($date);
		$tvshows[$row['id']] = $row;
		}
	return $tvshows;
	}
// Tv show lisamine
function addshow()
	{
	global $myurl;
	global $connection;
	// Kontrollime väljade täidetust
	if (isset($_POST['newshow']) && $_POST['newshow'] != "")
		{
		$tvshow_title = $_POST['newshow'];
		}
	  else
		{
		$tverrors[] = "Nimi puudub!";
		}
	//Kui vigu polnud, lisame nimetuse
	if (empty($tverrors))
		{
		$result = add_tvshow($tvshow_title);
		if ($result)
			{
			header("Location: $myurl?mode=review");
			exit(0);
			}
		}
	  else
		{
		// Kui on vead, kuva need
		include ("view/review.html");

		}
	}
// Kasutaja lisatud tvshow lisamine andmebaasi
function add_tvshow($tvshow_title)
	{
	global $connection;
	global $myurl;
	$userid = $_SESSION['user_id'];
	$tvshow_title = mysqli_real_escape_string($connection, $tvshow_title);
	$query = "insert into jjakimet_tvshows (user_id, tvshow_title) VALUES ($userid, '$tvshow_title')";
	mysqli_query($connection, $query) or die("$query - " . mysqli_error($connection));
	return mysqli_insert_id($connection);
	}
// Lisatud tvshow kustutamine
function delete_tvshow()
	{
	global $connection;
	global $myurl;
	$tvshow_id = $_GET['id'];
	$userid = $_SESSION['user_id'];
	$tvshow_id = mysqli_real_escape_string($connection, $tvshow_id);
	$query = "delete from jjakimet_tvshows where id=$tvshow_id and user_id=$userid";
	mysqli_query($connection, $query) or die("$query - " . mysqli_error($connection));
	header("Location: $myurl?mode=review");
	}
// logi välja
function logout()
	{
	global $myurl;
	// kustuta sessioon ja suuna kasutaja pealehele
	endsession();
	header("Location: ?mode=movies");
	}
?>
