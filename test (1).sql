-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 09, 2015 at 02:08 AM
-- Server version: 5.5.43
-- PHP Version: 5.3.10-1ubuntu3.18

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `jjakimet_kasutajad`
--

CREATE TABLE IF NOT EXISTS `jjakimet_kasutajad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_estonian_ci DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8_estonian_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_estonian_ci DEFAULT NULL,
  `pass` varchar(100) COLLATE utf8_estonian_ci DEFAULT NULL,
  `privileges` enum('admin','user') COLLATE utf8_estonian_ci DEFAULT 'user',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jjakimet_kasutajad`
--

INSERT INTO `jjakimet_kasutajad` (`id`, `username`, `firstname`, `lastname`, `pass`, `privileges`) VALUES
(1, 'jjakimet', 'katrin', 'jakimets', 'e48e2133b5afb8798339ff1bf29dbbd068dfb556', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
